" mouse suport
set mouse=a

" indent setup
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2

set nowrap
set nocompatible

" syntax and coloscheme
set t_Co=256
colorscheme desert256
command Light colorscheme pyte|highlight Normal ctermbg=white|highlight Normal ctermfg=black
command Dark colorscheme desert256

" space visualisation
set listchars=tab:»\ ,trail:·,nbsp:␣,precedes:<,extends:>
set list

" leader key
let g:mapleader = ","

" remap split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" # PLUGINS
call plug#begin('~/.vim/plugged')

" a universal set of defaults
Plug 'tpope/vim-sensible'

" number setup
Plug 'jeffkreeftmeijer/vim-numbertoggle'
set number

" status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-tsserver', { 'do': 'yarn install --frozen-lockfile'}

let g:vim_svelte_plugin_load_full_syntax = 1
let g:vim_svelte_plugin_use_typescript = 1
" Plug 'leafOfTree/vim-svelte-plugin'
Plug 'coc-extensions/coc-svelte', {'do': 'yarn install --frozen-lockfile'}

  "coc-browser >> a voir...
Plug 'neoclide/coc-css', {'do': 'yarn install --frozen-lockfile'}
Plug 'josa42/coc-docker', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-eslint', {'do': 'yarn install --frozen-lockfile'}
  "coc-git",
  "coc-graphql",
  "coc-html",
  "coc-json",
  "coc-markdownlint",
  "coc-prisma",
Plug 'neoclide/coc-snippets', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-tabnine', {'do': 'yarn install --frozen-lockfile'}
Plug 'fannheyward/coc-deno', {'do': 'yarn install --frozen-lockfile'}

inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

" fugitive
Plug 'tpope/vim-fugitive'

" language packs
Plug 'sheerun/vim-polyglot'

" linter
Plug 'w0rp/ale'
"let g:airline#extensions#ale#enabled = 1
let g:ale_linters = {'javascript': ['eslint'], 'typescript': ['eslint']}
let g:ale_fixers = {'javascript': ['eslint'], 'typescript': ['eslint']}
let g:ale_linter_aliases = {'javascript.jsx': ['javascript']}
let g:ale_lint_on_enter = 1
let g:ale_fix_on_save = 1

" tmux
Plug 'christoomey/vim-tmux-navigator'
let g:tmux_navigator_no_mappings = 1
let g:tmux_navigator_save_on_switch = 2 " write all buffers

nnoremap <silent> <C-H> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-J> :TmuxNavigateDown<cr>
nnoremap <silent> <C-K> :TmuxNavigateUp<cr>
nnoremap <silent> <C-L> :TmuxNavigateRight<cr>

" local vim configuration
Plug 'editorconfig/editorconfig-vim'

" activity-watch
Plug 'ActivityWatch/aw-watcher-vim'

" languagetool
Plug 'dpelle/vim-LanguageTool'
let g:languagetool_cmd='/usr/bin/languagetool'

" emmet
Plug 'mattn/emmet-vim'

call plug#end()

if !empty(system("setxkbmap -print|grep bepo"))
    source ~/.vim/vimrc.bepo
endif
